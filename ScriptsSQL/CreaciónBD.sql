/*Creación de la base de datos*/
create database ProyectoAjedrez;

/*Tabla login*/
create table Login(
id int primary key auto_increment,
usuario varchar (50),
psswdr varchar (50));

/*Tabla Hotel*/
create table Hotel(
idhotel int primary key not null,
nombre varchar (50),
direccion varchar (100),
telefono varchar (10));

/*Tabla Sala*/
create table Sala(
idsala int primary key not null,
fkidhotel int,
foreign key (fkidhotel) references Hotel (idhotel),
capacidad int (10),
medios varchar (1000));

/*Tabla País*/
create table Pais(
idpais int primary key not null,
nombre varchar(20),
numeroClubs int (10),
representa int);

/*Tabla Participante*/
create table Participante(
nsocio int primary key not null,
nombre varchar (50),
direccion varchar (50),
telefono varchar (10),
fkpais int,
foreign key (fkpais) references Pais (idpais));

/*Tabla Reserva*/
create table Reserva(
idreserva int primary key not null,
fechaentrada date,
fechasalida date,
fkidhotel int,
foreign key (fkidhotel) references Hotel (idhotel),
fkidparticipante int,
foreign key (fkidparticipante) references Participante (nsocio));

/*Tabla Arbitro*/
create table Arbitro(
idarbitro int primary key not null,
foreign key (idarbitro) references Participante (nsocio));

/*Tabla Jugador*/
create table Jugador(
idjugador int primary key not null,
foreign key (idjugador) references Participante (nsocio),
nivel int(10));

/*Tabla Campeonato*/
create table Campeonato(
idcampeonato int primary key not null,
nombre varchar (100),
tipo varchar (100),
fkidparticipante int,
foreign key (fkidparticipante) references Participante (nsocio));

/*Tabla Partida*/
create table Partida(
idpartida int primary key not null,
jornada datetime,
fkidarbitro int,
foreign key (fkidarbitro) references Arbitro (idarbitro),
fkidhotel int,
foreign key (fkidhotel) references Hotel (idhotel),
fkidsala int,
foreign key (fkidsala) references Sala (idsala),
entradas int);

/*Tabla Movimientos*/
create table Movimientos(
idpartida int not null,
idmovimiento int not null,
movimiento varchar (50),
comentario varchar (200),
primary key (idpartida, idmovimiento));

/*Tabla Juego*/
create table Juego(
idpartida int not null,
idjugador int not null,
color varchar (20),
primary key (idpartida, idjugador));