﻿
namespace Entidades.Ajedrez
{
    public class Participante
    {
        public int Nsocio { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
        public int FkPais { get; set; }

        public Participante(int nsocio, string nombre, string direccion, string telefono, int fkPais)
        {
            Nsocio = nsocio;
            Nombre = nombre;
            Direccion = direccion;
            Telefono = telefono;
            FkPais = fkPais;
        }
    }
}
