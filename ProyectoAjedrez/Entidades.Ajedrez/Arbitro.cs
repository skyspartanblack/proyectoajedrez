﻿
namespace Entidades.Ajedrez
{
    public class Arbitro
    {
        public int IdArbitro { get; set; }

        public Arbitro(int idArbitro)
        {
            IdArbitro = idArbitro;
        }
    }
}
