﻿namespace Entidades.Ajedrez
{
    public class Juegos
    {
        public int Idpartida { get; set; }
        public int IdJugador { get; set; }
        public string Color { get; set; }

        public Juegos(int idpartida, int idJugador, string color)
        {
            Idpartida = idpartida;
            IdJugador = idJugador;
            Color = color;
        }
    }
}
