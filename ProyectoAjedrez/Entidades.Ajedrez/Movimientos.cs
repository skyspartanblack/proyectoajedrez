﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Ajedrez
{
    public class Movimientos
    {
        public int IdPartida { get; set; }
        public int IdMovimiento { get; set; }
        public string Movimiento { get; set; }
        public string Comentario { get; set; }
        public Movimientos(int idpartida, int idmovimiento, string movimiento, string comentario)
        {
            IdMovimiento = idmovimiento;
            IdPartida = idpartida;
            Movimiento = movimiento;
            Comentario = comentario;
        }
    }
}
