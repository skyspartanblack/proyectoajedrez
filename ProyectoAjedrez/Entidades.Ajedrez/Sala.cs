﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Ajedrez
{
    public class Sala
    {
        public int IdSala { get; set; }
        public int FkIdHotel { get; set; }
        public int Capacidad { get; set; }
        public string Medios { get; set; }
        public Sala(int idsala, int fkidhotel, int capacidad, string medios)
        {
            IdSala = idsala;
            FkIdHotel = fkidhotel;
            Capacidad = capacidad;
            Medios = medios;
        }
    }
}
