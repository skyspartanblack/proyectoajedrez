﻿
namespace Entidades.Ajedrez
{
    public class Paises
    {
        public int Idpais { get; set; }
        public string Nombre { get; set; }
        public int NumeroClubs { get; set; }
        public int Representa { get; set; }

        public Paises(int idpais, string nombre, int numeroClubs, int representa)
        {
            Idpais = idpais;
            Nombre = nombre;
            NumeroClubs = numeroClubs;
            Representa = representa;
        }
    }
}
