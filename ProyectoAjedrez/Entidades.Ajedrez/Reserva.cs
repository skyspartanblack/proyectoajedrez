﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidades.Ajedrez
{
    public class Reserva
    {
        public int IdReserva { get; set; }
        public string FechaEntrada { get; set; }
        public string FechaSalida { get; set; }
        public int FkIdHotel { get; set; }
        public int FkIdParticipante { get; set; }
        public Reserva(int idreserva, string fechaentrada, string fechasalida, int fkidhotel, int fkidparticipante)
        {
            IdReserva = idreserva;
            FechaEntrada = fechaentrada;
            FechaSalida = fechasalida;
            FkIdHotel = fkidhotel;
            FkIdParticipante = fkidparticipante;
        }
    }
}
