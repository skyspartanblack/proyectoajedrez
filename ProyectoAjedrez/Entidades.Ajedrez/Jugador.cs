﻿
namespace Entidades.Ajedrez
{
    public class Jugador
    {
        public int IdJugador { get; set; }
        public int Nivel { get; set; }

        public Jugador(int idJugador, int nivel)
        {
            IdJugador = idJugador;
            Nivel = nivel;
        }
    }
}
