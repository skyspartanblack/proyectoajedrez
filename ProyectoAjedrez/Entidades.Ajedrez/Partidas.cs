﻿namespace Entidades.Ajedrez
{
    public class Partidas
    {
        public int IdPartida { get; set; }
        public string Jornada { get; set; }
        public int FkIDarbitro { get; set; }
        public int FkIDHotel { get; set; }
        public int FkIDSala { get; set; }
        public int Entradas { get; set; }
        public Partidas(int idPartida, string jornada, int fkIDarbitro, int fkIDHotel, int fkIDSala, int entradas)
        {
            IdPartida = idPartida;
            Jornada = jornada;
            FkIDarbitro = fkIDarbitro;
            FkIDHotel = fkIDHotel;
            FkIDSala = fkIDSala;
            Entradas = entradas;
        }
    }
}
