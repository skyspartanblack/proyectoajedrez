﻿using System.Data;
using System.Windows.Forms;
using Entidades.Ajedrez;

namespace AccesoDatos.Ajedrez
{
    public class AccesoDatosParticipante
    {
        ConexionBDAjedrez ca = new ConexionBDAjedrez();

        public int GetIdPais(string pais)
        {
            DataTable dt = new DataTable();
            dt = ca.Mostrar(string.Format("select idpais from Pais where nombre = '{0}'",
                pais), "Pais").Tables[0];
            DataRow r = dt.Rows[0];
            return int.Parse(r["idpais"].ToString());
        }

        public string GetNombrePais(int idpais)
        {
            DataTable dt = new DataTable();
            dt = ca.Mostrar(string.Format("select nombre from Pais where idpais = {0}",
                idpais), "Pais").Tables[0];
            DataRow r = dt.Rows[0];
            return r["nombre"].ToString();
        }

        public string Insertar(Participante participantes)
        {
            return ca.Comando(string.Format("insert into Participante values({0},'{1}','{2}','{3}',{4})",
                participantes.Nsocio, participantes.Nombre, participantes.Direccion, participantes.Telefono, participantes.FkPais));
        }

        public string Borrar(Participante participantes)
        {
            return ca.Comando(string.Format("delete from Participante where Nsocio = {0}", participantes.Nsocio));
        }

        public string Actualizar(Participante participantes)
        {
            return ca.Comando(string.Format("update Participante set nombre = '{0}', direccion = '{1}', telefono = '{2}', fkpais = {3} where nsocio = {4}",
                participantes.Nombre, participantes.Direccion, participantes.Telefono, participantes.FkPais, participantes.Nsocio));
        }

        public DataSet Listado(string q, string tabla)
        {
            return ca.Mostrar(q, tabla);
        }

        public void LlenarPais(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "nombre";
        }
    }
}
