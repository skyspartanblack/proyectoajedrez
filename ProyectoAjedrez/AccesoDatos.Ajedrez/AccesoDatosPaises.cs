﻿using Entidades.Ajedrez;
using System.Data;
using System.Windows.Forms;

namespace AccesoDatos.Ajedrez
{
    public class AccesoDatosPaises
    {
        ConexionBDAjedrez _conexion = new ConexionBDAjedrez();
        public string GuardarPais(Paises pais)
        {
            return _conexion.Comando(string.Format("insert into pais values({0},'{1}',{2},{3})", pais.Idpais, pais.Nombre, pais.NumeroClubs, pais.Representa));
        }
        public string ActualizarPais(Paises pais)
        {
            return _conexion.Comando(string.Format("update pais set nombre = '{0}', numeroClubs = {1}, representa = {2} where idpais = {3}", pais.Nombre, pais.NumeroClubs, pais.Representa, pais.Idpais));
        }
        public string EliminarPais(Paises pais)
        {
            return _conexion.Comando(string.Format("delete from pais where idpais = {0}", pais.Idpais));
        }
        public DataSet ObtenerPaises(string consulta, string tabla)
        {
            return _conexion.Mostrar(consulta, tabla);
        }
        //aqui necesitamos sacar la id del pais podemos hacerlo con el nombre del pais
        public int GetIdPais(string nombre)
        {
            DataTable dt = new DataTable();
            dt = _conexion.Mostrar(string.Format("select idpais from pais where nombre = '{0}'", nombre), "pais").Tables[0];
            DataRow r = dt.Rows[0];
            return int.Parse(r["idpais"].ToString());
        }
        //aqui necesitamos sacar el nombre del pais, podemos hacerlo con la id del pais
        public string GetPais(int idpais)
        {
            DataTable dt = new DataTable();
            dt = _conexion.Mostrar(string.Format("select nombre from pais where idpais = {0}", idpais), "pais").Tables[0];
            DataRow r = dt.Rows[0];
            return r["nombre"].ToString();
        }
        //metodo para llenar comboox
        public void LlenarPaises(ComboBox combo, string consulta, string tabla)
        {
            combo.DataSource = _conexion.Mostrar(consulta, tabla).Tables[0];
            combo.DisplayMember = "nombre";
        }
    }
}
