﻿using Entidades.Ajedrez;
using System.Data;
using System.Windows.Forms;

namespace AccesoDatos.Ajedrez
{
    public class AccesoDatosPartidas
    {
        ConexionBDAjedrez _conexion = new ConexionBDAjedrez();
        public string GuardarPartida(Partidas partida)
        {
            return _conexion.Comando(string.Format("insert into partida values ({0},'{1}',{2},{3},{4},{5})", partida.IdPartida, partida.Jornada, partida.FkIDarbitro, partida.FkIDHotel, partida.FkIDSala, partida.Entradas));
        }
        public string ActualizarPartida(Partidas partida)
        {
            return _conexion.Comando(string.Format("update partida set jornada = '{0}',fkidarbitro = {1}, fkidhotel ={2},fkidsala ={3},entradas = {4}" +
                " where idpartida = {5}", partida.Jornada, partida.FkIDarbitro, partida.FkIDHotel, partida.FkIDSala, partida.Entradas, partida.IdPartida));
        }
        public string EliminarPartida(Partidas partida)
        {
            return _conexion.Comando(string.Format("delete from partida where idpartida = {0}", partida.IdPartida));
        }
        public DataSet ObtenerPartidas(string consulta, string tabla)
        {
            return _conexion.Mostrar(consulta, tabla);
        }

        public void LlenarArbitro(ComboBox combo, string consulta, string tabla)
        {
            combo.DataSource = _conexion.Mostrar(consulta, tabla).Tables[0];
            combo.DisplayMember = "idarbitro";
        }
        public void LlenarHotel(ComboBox combo, string consulta, string tabla)
        {
            combo.DataSource = _conexion.Mostrar(consulta, tabla).Tables[0];
            combo.DisplayMember = "idhotel";
        }
        public void LLenarSala(ComboBox combo, string consulta, string tabla)
        {
            combo.DataSource = _conexion.Mostrar(consulta, tabla).Tables[0];
            combo.DisplayMember = "idsala";
        }
    }
}
