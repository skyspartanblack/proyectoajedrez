﻿using System;
using System.Data;

namespace AccesoDatos.Ajedrez
{
    public class AccesoDatosLogin
    {
        ConexionBDAjedrez c = new ConexionBDAjedrez();
        public string ValidarUsuario(string usuario)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = c.Mostrar(string.Format("select usuario from Login where usuario = '{0}'", usuario), "Login").Tables[0];
                DataRow r = dt.Rows[0];
                return r["usuario"].ToString();
            }
            catch (Exception)
            {
                return "false";
            }
        }

        public string ValidarContraseña(string contraseña)
        {
            try
            {
                DataTable dt = new DataTable();
                dt = c.Mostrar(string.Format("select psswdr from Login where psswdr = '{0}'", contraseña), "Login").Tables[0];
                DataRow r = dt.Rows[0];
                return r["psswdr"].ToString();
            }
            catch (Exception)
            {
                return "false";
            }
        }
    }
}
