﻿using System.Data;
using System.Windows.Forms;
using Entidades.Ajedrez;

namespace AccesoDatos.Ajedrez
{
    public class AccesoDatosMovimientos
    {
        ConexionBDAjedrez ca = new ConexionBDAjedrez();
        public string Insertar(Movimientos mov)
        {
            return ca.Comando(string.Format("insert into Movimientos values({0}, {1},'{2}','{3}')",
                mov.IdPartida, mov.IdMovimiento, mov.Movimiento, mov.Comentario));
        }
        public string Borrar(Movimientos mov)
        {
            return ca.Comando(string.Format("delete from Movimientos where idmovimiento = {0}", mov.IdMovimiento));
        }
        public string Actualizar(Movimientos mov)
        {
            return ca.Comando(string.Format("update Movimientos set idpartida = {0}, movimiento = '{1}', comentario = '{2}' where idmovimiento = {3} ", mov.IdPartida, mov.Movimiento, mov.Comentario, mov.IdMovimiento));
        }
        public DataSet Listado(string q, string tabla)
        {
            return ca.Mostrar(q, tabla);
        }

        public void llenarComboPartida(ComboBox combo, string script, string tabla)
        {
            combo.DataSource = Listado(script, tabla).Tables[0];
            combo.DisplayMember = "idpartida";
        }
    }
}