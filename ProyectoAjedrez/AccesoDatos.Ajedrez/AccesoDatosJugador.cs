﻿using System.Data;
using System.Windows.Forms;
using Entidades.Ajedrez;

namespace AccesoDatos.Ajedrez
{
    public class AccesoDatosJugador
    {
        ConexionBDAjedrez ca = new ConexionBDAjedrez();

        public int GetIdParticipante(string Participante)
        {
            DataTable dt = new DataTable();
            dt = ca.Mostrar(string.Format("select nsocio from Participante where nombre = '{0}'",
                Participante), "Participante").Tables[0];
            DataRow r = dt.Rows[0];
            return int.Parse(r["nsocio"].ToString());
        }

        public string GetParticipante(int nsocio)
        {
            DataTable dt = new DataTable();
            dt = ca.Mostrar(string.Format("select nombre from Participante where nsocio = {0}",
                nsocio), "Participante").Tables[0];
            DataRow r = dt.Rows[0];
            return r["nombre"].ToString();
        }

        public string Insertar(Jugador jugadores)
        {
            return ca.Comando(string.Format("insert into Jugador values({0},{1})",
                jugadores.IdJugador, jugadores.Nivel));
        }

        public string Borrar(Jugador jugadores)
        {
            return ca.Comando(string.Format("delete from Jugador where idjugador = {0}", jugadores.IdJugador));
        }

        public string Actualizar(Jugador jugadores)
        {
            return ca.Comando(string.Format("update Jugador set nivel = {0} where idjugador = {1}",
                jugadores.Nivel, jugadores.IdJugador));
        }

        public DataSet Listado(string q, string tabla)
        {
            return ca.Mostrar(q, tabla);
        }

        public void LlenarParticipantes(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "nsocio";
        }
    }
}
