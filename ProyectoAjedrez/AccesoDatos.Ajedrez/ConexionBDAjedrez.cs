﻿using System.Data;
using Bases;

namespace AccesoDatos.Ajedrez
{
    public class ConexionBDAjedrez
    {
        Conectar c = new Conectar("localhost", "root", "", "ProyectoAjedrez");
        public string Comando(string s)
        {
            return c.Comando(s);
        }

        public DataSet Mostrar(string s, string tabla)
        {
            return c.Consultar(s, tabla);
        }
    }
}
