﻿using System.Data;
using System.Windows.Forms;
using Entidades.Ajedrez;

namespace AccesoDatos.Ajedrez
{
    public class AccesoDatosArbitro
    {
        ConexionBDAjedrez ca = new ConexionBDAjedrez();
        public string GetParticipante(int nsocio)
        {
            DataTable dt = new DataTable();
            dt = ca.Mostrar(string.Format("select nombre from Participante where nsocio = {0}",
                nsocio), "Participante").Tables[0];
            DataRow r = dt.Rows[0];
            return r["nombre"].ToString();
        }

        public string Insertar(Arbitro arbitros)
        {
            return ca.Comando(string.Format("insert into Arbitro values({0})",
                arbitros.IdArbitro));
        }

        public string Borrar(Arbitro arbitros)
        {
            return ca.Comando(string.Format("delete from Arbitro where idarbitro = {0}", arbitros.IdArbitro));
        }

        public DataSet Listado(string q, string tabla)
        {
            return ca.Mostrar(q, tabla);
        }

        public void LlenarParticipantes(ComboBox combo, string q, string tabla)
        {
            combo.DataSource = ca.Mostrar(q, tabla).Tables[0];
            combo.DisplayMember = "nsocio";
        }
    }
}
