﻿using System;
using System.Windows.Forms;
using Entidades.Ajedrez;
using LogicaDeNegocio.Ajedrez;

namespace Presentaciones.Ajedrez
{
    public partial class FrmJuegos : Form
    {
        int fila = 0;
        LogicaJuegos jm = new LogicaJuegos();
        public FrmJuegos()
        {
            InitializeComponent();
        }

        private void FrmJuegos_Load(object sender, EventArgs e)
        {
            lblindicacion.Visible = false;
            BtnModificar.Enabled = false;
            BtnEliminar.Enabled = false;
            Actualizar();
            try
            {
                jm.LlenarPartida(CmbIdPartida, "select idpartida from partida", "partida");
                jm.LlenarJugador(CmbJugador, "select idjugador from jugador", "jugador");
            }
            catch (Exception)
            {

                this.Show();
            }          
        }
        public void Limpiar()
        {
            CmbColor.SelectedItem = null;
            CmbIdPartida.SelectedItem = null;
            CmbJugador.SelectedItem = null;
        }
        public void Actualizar()
        {
            DtgJuegos.DataSource = jm.Obtener(string.Format("select * from juego where idpartida like '%{0}%'", TxtBuscar.Text), "Juego").Tables[0];
            DtgJuegos.AutoResizeColumns();
            DtgJuegos.AutoResizeRows();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                jm.Guardar(new Juegos(int.Parse(CmbIdPartida.Text), int.Parse(CmbJugador.Text), CmbColor.Text));
                Actualizar();
                Limpiar();
            }
            catch (Exception)
            {

                MessageBox.Show("Caracteres invalidos");
            }
            
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                CmbIdPartida.Enabled = true;
                BtnGuardar.Enabled = true;
                jm.Actualizar(new Juegos(int.Parse(CmbIdPartida.Text), int.Parse(CmbJugador.Text), CmbColor.Text));
                Actualizar();
                Limpiar();
                BtnModificar.Enabled = false;
                BtnEliminar.Enabled = false;
                lblindicacion.Visible = false;
            }
            catch (Exception)
            {

                MessageBox.Show("No se realizo ningun cambio, error de dato");
                BtnModificar.Enabled = false;
                lblindicacion.Visible = false;
                BtnEliminar.Enabled = false;
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult rs = MessageBox.Show("¿Esta seguro de eliminar el juego de la partida " + CmbIdPartida.Text + "?",
                "Atencion!!!", MessageBoxButtons.YesNoCancel);
                if (rs == DialogResult.Yes)
                {
                    jm.Eliminar(new Juegos(int.Parse(CmbIdPartida.Text), 0, ""));
                    Limpiar();
                    Actualizar();
                    BtnEliminar.Enabled = false;
                    BtnGuardar.Enabled = true;
                    BtnModificar.Enabled = false;
                    CmbIdPartida.Enabled = true;
                    lblindicacion.Visible = false;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Antes de eliminar necesitas hacer click en el dato que deseas eliminar.");
                BtnGuardar.Enabled = true;
                BtnEliminar.Enabled = false;
                BtnModificar.Enabled = false;
                CmbIdPartida.Enabled = true;
                lblindicacion.Visible = false;
            }
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void DtgJuegos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            CmbIdPartida.Enabled = false;
            BtnGuardar.Enabled = false;
            BtnModificar.Enabled = true;
            BtnEliminar.Enabled = true;
            lblindicacion.Visible = true;
            try
            {
                fila = e.RowIndex;
                CmbIdPartida.Text = DtgJuegos.Rows[fila].Cells[0].Value.ToString();
                CmbJugador.Text = DtgJuegos.Rows[fila].Cells[1].Value.ToString();
                CmbColor.Text = DtgJuegos.Rows[fila].Cells[2].Value.ToString();
            }
            catch (Exception)
            {

                MessageBox.Show("Para actualizar y eliminar da clic sobre el dato que deseas manipular");
            }
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            Actualizar();
        }
    }
}
