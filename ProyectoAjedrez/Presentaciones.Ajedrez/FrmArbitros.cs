﻿using System;
using System.Windows.Forms;
using Entidades.Ajedrez;
using LogicaDeNegocio.Ajedrez;

namespace Presentaciones.Ajedrez
{
    public partial class FrmArbitros : Form
    {
        LogicaArbitro al = new LogicaArbitro();
        int fila = 0;
        int posicion;
        public FrmArbitros()
        {
            InitializeComponent();
        }

        void Actualizar()
        {
            DtgArbitros.DataSource = al.Listado(string.Format(
                "select idarbitro as IDArbitro from Arbitro where idarbitro like '%{0}%'",
                TxtBuscar.Text), "Arbitro").Tables[0];
            DtgArbitros.AutoResizeColumns();
        }

        void Limpiar()
        {
            CmbIdArbitro.Text = "";
            TxtNombre.Clear();
        }

        private void FrmArbitros_Load(object sender, EventArgs e)
        {
            Actualizar();
            al.LlenarParticipantes(CmbIdArbitro, "select nsocio from Participante", "Participante");
            BtnEliminar.Enabled = false;
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void DtgArbitros_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = e.RowIndex;
            CmbIdArbitro.Text = DtgArbitros.Rows[fila].Cells[0].Value.ToString();
            TxtNombre.Text = al.GetParticipante(int.Parse(CmbIdArbitro.Text));
            BtnGuardar.Enabled = false;
            BtnEliminar.Enabled = true;
            CmbIdArbitro.Enabled = false;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            al.Insertar(new Arbitro(int.Parse(CmbIdArbitro.Text)));
            Actualizar();
            Limpiar();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult rs = MessageBox.Show("¿Esta seguro de eliminar al arbitro " + TxtNombre.Text + "?",
                "Atencion!!!", MessageBoxButtons.YesNoCancel);
                if (rs == DialogResult.Yes)
                {
                    al.Borrar(new Arbitro(int.Parse(CmbIdArbitro.Text)));
                    Limpiar();
                    Actualizar();
                    BtnGuardar.Enabled = true;
                    BtnEliminar.Enabled = false;
                    CmbIdArbitro.Enabled = true;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Antes de eliminar necesitas hacer click en el dato que deseas eliminar.");
            }
        }

        void Asignar()
        {
            string x = CmbIdArbitro.Text;
            if (!x.Equals("System.Data.DataRowView"))
            {
                posicion = int.Parse(CmbIdArbitro.Text);
            }
        }

        private void CmbIdArbitro_SelectedIndexChanged(object sender, EventArgs e)
        {
            Asignar();
            if (posicion > 0)
            {
                TxtNombre.Text = al.GetParticipante(int.Parse(CmbIdArbitro.Text));
            }
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
