﻿using System;
using System.Windows.Forms;
using Entidades.Ajedrez;
using LogicaDeNegocio.Ajedrez;

namespace Presentaciones.Ajedrez
{
    public partial class FrmReservas : Form
    {
        LogicaReserva lr = new LogicaReserva();
        int fila = 0;
        public FrmReservas()
        {
            InitializeComponent();
        }
        void Actualizar()
        {
            DtgReserva.DataSource = lr.Mostrar(string.Format("select idreserva as IdReserva, fechaentrada as FechaEntrada, fechasalida as FechaSalida, fkidhotel as Hotel, fkidparticipante as Participante from Reserva where idreserva like '%{0}%'",
                txtBuscar.Text), "Reserva").Tables[0];
            DtgReserva.AutoResizeColumns();

        }
        public void Limpiar()
        {
            txtIdreserva.Clear();
            CmbHotel.Text = "";
            CmbParticipante.Text = "";
            txtBuscar.Clear();
        }
        private void FrmReservas_Load(object sender, EventArgs e)
        {
            lr.llenarCombo(CmbHotel, "Select Nombre from Hotel", "Hotel");
            lr.llenarCombo2(CmbParticipante, "Select Nombre from Participante", "Participante");
            CmbHotel.Text = "";
            CmbParticipante.Text = "";
            Actualizar();
            BtnEliminar.Enabled = false;
            BtnActualizar.Enabled = false;
        }

        private void txtBuscar_TextChanged(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void DtgReserva_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BtnGuardar.Enabled = false;
            txtIdreserva.Enabled = false;
            BtnActualizar.Enabled = true;
            BtnEliminar.Enabled = true;

            fila = e.RowIndex;
            txtIdreserva.Text = DtgReserva.Rows[fila].Cells[0].Value.ToString();
            Dtmentrada.Text = DtgReserva.Rows[fila].Cells[1].Value.ToString();
            DtmSalida.Text = DtgReserva.Rows[fila].Cells[2].Value.ToString();
            CmbHotel.Text = lr.ObtenerNombrehotel(int.Parse(DtgReserva.Rows[fila].Cells[3].Value.ToString()));
            CmbParticipante.Text = lr.ObtenerNombreparticipante(int.Parse(DtgReserva.Rows[fila].Cells[4].Value.ToString()));
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                int hotel = lr.Obteneridhotel(CmbHotel.Text);
                int participante = lr.ObtenerIdparticipante(CmbParticipante.Text);
                lr.Insertar(new Reserva(int.Parse(txtIdreserva.Text), Dtmentrada.Text, DtmSalida.Text, hotel, participante));
                Actualizar();
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                int hotel = lr.Obteneridhotel(CmbHotel.Text);
                int participante = lr.ObtenerIdparticipante(CmbParticipante.Text);
                lr.Actualizar(new Reserva(int.Parse(txtIdreserva.Text), Dtmentrada.Text, DtmSalida.Text, hotel, participante));
                Actualizar();
                Limpiar();
                BtnGuardar.Enabled = true;
                txtIdreserva.Enabled = true;
                BtnActualizar.Enabled = false;
                BtnEliminar.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Debe seleccionar el dato a actualizar");
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult rs = MessageBox.Show("¿Estas seguro de eliminar la reserva " + txtIdreserva.Text + " ?", "¡¡Importante!!", MessageBoxButtons.YesNoCancel);
                if (rs == DialogResult.Yes)
                {
                    lr.Eliminar(new Reserva(int.Parse(txtIdreserva.Text), "", "", 0, 0));
                    Limpiar();
                    Actualizar();
                    BtnGuardar.Enabled = true;
                    txtIdreserva.Enabled = true;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Debe seleccionar el dato a eliminar");
            }
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}