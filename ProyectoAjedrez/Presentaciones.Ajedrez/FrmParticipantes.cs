﻿using System;
using Entidades.Ajedrez;
using LogicaDeNegocio.Ajedrez;
using System.Windows.Forms;

namespace Presentaciones.Ajedrez
{
    public partial class FrmParticipantes : Form
    {
        LogicaParticipante pln = new LogicaParticipante();
        int fila = 0;
        public FrmParticipantes()
        {
            InitializeComponent();
        }

        void Actualizar()
        {
            DtgParticipantes.DataSource = pln.Mostrar(string.Format(
                "select nsocio as NSocio, nombre as Nombre, direccion as Direccion, telefono as Telefono, fkpais as Pais from Participante where nombre like '%{0}%'",
                TxtBuscar.Text), "Participante").Tables[0];
            DtgParticipantes.AutoResizeColumns();
        }

        void Limpiar()
        {
            TxtNsocio.Clear();
            TxtNombre.Clear();
            TxtDireccion.Clear();
            TxtTelefono.Clear();
        }

        private void FrmParticipantes_Load(object sender, EventArgs e)
        {
            Actualizar();
            pln.LlenarPais(CmbPais, "select nombre from Pais", "Pais");
            CmbPais.Text = "";
            BtnEliminar.Enabled = false;
            BtnModificar.Enabled = false;
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void DtgParticipantes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = e.RowIndex;
            TxtNsocio.Text = DtgParticipantes.Rows[fila].Cells[0].Value.ToString();
            TxtNombre.Text = DtgParticipantes.Rows[fila].Cells[1].Value.ToString();
            TxtDireccion.Text = DtgParticipantes.Rows[fila].Cells[2].Value.ToString();
            TxtTelefono.Text = DtgParticipantes.Rows[fila].Cells[3].Value.ToString();
            CmbPais.Text = pln.GetNombrePais(int.Parse(DtgParticipantes.Rows[fila].Cells[4].Value.ToString()));
            BtnGuardar.Enabled = false;
            BtnEliminar.Enabled = true;
            BtnModificar.Enabled = true;
            TxtNsocio.Enabled = false;
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            int FkPais = pln.GetIdPais(CmbPais.Text);
            pln.Insertar(new Participante(int.Parse(TxtNsocio.Text), TxtNombre.Text, TxtDireccion.Text, TxtTelefono.Text, FkPais));
            Actualizar();
            Limpiar();
        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                int FkPais = pln.GetIdPais(CmbPais.Text);
                pln.Actualizar(new Participante(int.Parse(TxtNsocio.Text), TxtNombre.Text, TxtDireccion.Text, TxtTelefono.Text, FkPais));
                Actualizar();
                Limpiar();
                BtnGuardar.Enabled = true;
                BtnEliminar.Enabled = false;
                BtnModificar.Enabled = false;
                TxtNsocio.Enabled = true;
            }
            catch (Exception)
            {
                MessageBox.Show("Antes de actualizar, necesitas hacer click en el dato que deseas modificar.");
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult rs = MessageBox.Show("¿Esta seguro de eliminar al participante " + TxtNombre.Text + "?",
                "Atencion!!!", MessageBoxButtons.YesNoCancel);
                if (rs == DialogResult.Yes)
                {
                    pln.Borrar(new Participante(int.Parse(TxtNsocio.Text), "", "", "", 0));
                    Limpiar();
                    Actualizar();
                    BtnGuardar.Enabled = true;
                    BtnEliminar.Enabled = false;
                    BtnModificar.Enabled = false;
                    TxtNsocio.Enabled = true;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Antes de eliminar necesitas hacer click en el dato que deseas eliminar.");
            }
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
