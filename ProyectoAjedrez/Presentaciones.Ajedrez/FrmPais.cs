﻿using System.Windows.Forms;
using System;
using Entidades.Ajedrez;
using LogicaDeNegocio.Ajedrez;

namespace Presentaciones.Ajedrez
{
  
    public partial class FrmPais : Form
    {
        LogicaPais pm = new LogicaPais();
        int fila = 0;
        int idpais = 1;
        public FrmPais()
        {
            InitializeComponent();
        }
        void Actualizar()
        {
            DtgPaises.DataSource = pm.Obtener(string.Format("select idpais as IDPais, nombre as Nombre, numeroClubs as Clubs, representa as Representa from Pais where nombre like '%{0}%'", TxtBuscar.Text), "Pais").Tables[0];
            DtgPaises.AutoResizeColumns();
            DtgPaises.AutoResizeRows();
        }
        void Limpiar()
        {

            TxtId.Clear();
            TxtNombre.Clear();
            TxtNumClubs.Clear();
            CmbRepresenta.SelectedItem = 1;
        }

        private void FrmPais_Load(object sender, System.EventArgs e)
        {
            lblindicacion.Visible = false;
            BtnModificar.Enabled = false;
            BtnEliminar.Enabled = false;
            Actualizar();
            try
            {

                pm.Llenarcombo(CmbRepresenta, "select nombre from pais", "pais");
                CmbRepresenta.Text = pm.ObtenerNombrePais(idpais);
            }
            catch (Exception)
            {

                this.Show();
            }
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void DtgPaises_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            TxtId.Enabled = false;
            BtnGuardar.Enabled = false;
            BtnModificar.Enabled = true;
            BtnEliminar.Enabled = true;
            lblindicacion.Visible = true;
            try
            {
                fila = e.RowIndex;
                TxtId.Text = DtgPaises.Rows[fila].Cells[0].Value.ToString();
                TxtNombre.Text = DtgPaises.Rows[fila].Cells[1].Value.ToString();
                TxtNumClubs.Text = DtgPaises.Rows[fila].Cells[2].Value.ToString();
                //CmbRepresenta.Text = DtgPaises.Rows[fila].Cells[3].Value.ToString();
                CmbRepresenta.Text = pm.ObtenerNombrePais(int.Parse(DtgPaises.Rows[fila].Cells[3].Value.ToString()));
            }
            catch (Exception)
            {

                MessageBox.Show("Antes de actualizar, necesitas hacer click en el dato que deseas modificar.");
            }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                int representa = pm.ObtenerIDPais(CmbRepresenta.Text);
                pm.Guardar(new Paises(int.Parse(TxtId.Text), TxtNombre.Text, int.Parse(TxtNumClubs.Text), representa));
                Actualizar();
                Limpiar();
            }
            catch (Exception)
            {

                MessageBox.Show("Caractéres invalidos");
            }

        }

        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                int representa = pm.ObtenerIDPais(CmbRepresenta.Text);
                TxtId.Enabled = true;
                BtnGuardar.Enabled = true;
                pm.Actualizar(new Paises(int.Parse(TxtId.Text), TxtNombre.Text, int.Parse(TxtNumClubs.Text), representa));
                Actualizar();
                Limpiar();
                BtnModificar.Enabled = false;
                BtnEliminar.Enabled = false;
                lblindicacion.Visible = false;
            }
            catch (Exception)
            {
                MessageBox.Show("No se realizo ningun cambio");
                TxtId.Enabled = true;
                BtnGuardar.Enabled = true;
                Limpiar();
                BtnModificar.Enabled = false;
                lblindicacion.Visible = false;
                BtnEliminar.Enabled = false;
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {

            try
            {
                DialogResult rs = MessageBox.Show("¿Esta seguro de eliminar al País " + TxtNombre.Text + "?",
                "Atencion!!!", MessageBoxButtons.YesNoCancel);
                if (rs == DialogResult.Yes)
                {
                    pm.Eliminar(new Paises(int.Parse(TxtId.Text), "", 0, 0));
                    Limpiar();
                    Actualizar();
                    BtnEliminar.Enabled = false;
                    BtnGuardar.Enabled = true;
                    BtnModificar.Enabled = false;
                    TxtId.Enabled = true;
                    lblindicacion.Visible = false;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Antes de eliminar necesitas hacer click en el dato que deseas eliminar.");
                BtnGuardar.Enabled = true;
                BtnEliminar.Enabled = false;
                BtnModificar.Enabled = false;
                TxtId.Enabled = true;
                lblindicacion.Visible = false;
            }
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
