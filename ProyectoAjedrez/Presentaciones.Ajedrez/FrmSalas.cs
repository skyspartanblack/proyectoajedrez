﻿using System;
using System.Windows.Forms;
using Entidades.Ajedrez;
using LogicaDeNegocio.Ajedrez;

namespace Presentaciones.Ajedrez
{
    public partial class FrmSalas : Form
    {
        LogicaSala ls = new LogicaSala();

        int fila = 0;
        public FrmSalas()
        {
            InitializeComponent();
        }
        void Actualizar()
        {
            DtgSala.DataSource = ls.Mostrar(string.Format("Select idsala as IdSala, fkidhotel as Hotel, capacidad as Capacidad, medios as Medios from sala where idsala like '%{0}%'",
                txtbuscar.Text), "Sala").Tables[0];
            DtgSala.AutoResizeColumns();
        }
        public void Limpiar()
        {
            txtbuscar.Clear();
            txtcapacidad.Clear();
            txtidsala.Clear();
            txtmedios.Clear();

        }
        private void FrmSalas_Load(object sender, EventArgs e)
        {
            ls.llenarCombo(Cmbhotel, "Select nombre from hotel", "hotel");
            Cmbhotel.Text = "";
            Actualizar();
            BtnActualizar.Enabled = false;
            BtnEliminar.Enabled = false;
        }

        private void txtbuscar_TextChanged(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void DtgSala_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BtnGuardar.Enabled = false;
            txtidsala.Enabled = false;
            BtnActualizar.Enabled = true;
            BtnEliminar.Enabled = true;

            fila = e.RowIndex;
            txtidsala.Text = DtgSala.Rows[fila].Cells[0].Value.ToString();
            Cmbhotel.Text = ls.ObtenerNombrehotel(int.Parse(DtgSala.Rows[fila].Cells[1].Value.ToString()));
            txtcapacidad.Text = DtgSala.Rows[fila].Cells[2].Value.ToString();
            txtmedios.Text = DtgSala.Rows[fila].Cells[3].Value.ToString();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                int hotel = ls.Obteneridhotel(Cmbhotel.Text);
                ls.Insertar(new Sala(int.Parse(txtidsala.Text), hotel, int.Parse(txtcapacidad.Text), txtmedios.Text));
                Actualizar();
                BtnActualizar.Enabled = false;
                BtnEliminar.Enabled = false;
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void BtnActualizar_Click(object sender, EventArgs e)
        {
            try
            {
                int hotel = ls.Obteneridhotel(Cmbhotel.Text);
                ls.Actualizar(new Sala(int.Parse(txtidsala.Text), hotel, int.Parse(txtcapacidad.Text), txtmedios.Text));
                Actualizar();
                Limpiar();
                BtnGuardar.Enabled = true;
                txtidsala.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Debe seleccionar el dato a actualizar");
            }
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult rs = MessageBox.Show("¿Estas seguro de eliminar la sala " + txtidsala.Text + " ?", "¡¡Importante!!", MessageBoxButtons.YesNoCancel);
                if (rs == DialogResult.Yes)
                {
                    ls.Eliminar(new Sala(int.Parse(txtidsala.Text), 0, 0, ""));
                    Limpiar();
                    Actualizar();
                    BtnGuardar.Enabled = true;
                    txtidsala.Enabled = true;

                }
            }
            catch (Exception)
            {
                MessageBox.Show("Debe seleccionar el dato a eliminar");
            }
        }

        private void BtnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}