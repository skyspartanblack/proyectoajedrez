﻿using System;
using System.Windows.Forms;
using Entidades.Ajedrez;
using LogicaDeNegocio.Ajedrez;

namespace Presentaciones.Ajedrez
{
    public partial class FrmMovimiento : Form
    {
        LogicaMovimientos _move = new LogicaMovimientos();
        int fila = 0;
        public FrmMovimiento()
        {
            InitializeComponent();
        }
        void Actualizar()
        {
            DtgMovimientos.DataSource = _move.Mostrar(string.Format(
                "select * from Movimientos where movimiento like '%{0}%'",
                TxtBuscar.Text), "Movimientos").Tables[0];
            DtgMovimientos.AutoResizeColumns();
        }
        void Limpiar()
        {
            txtComentario.Clear();
            cmbPartida.Text = "";
            txtIdMove.Clear();
            txtMovimiento.Clear();
        }
        void Unable(Boolean id, Boolean guardar, Boolean eliminar, Boolean editar)
        {
            BtnEliminar.Enabled = eliminar;
            BtnGuardar.Enabled = guardar;
            BtnModificar.Enabled = editar;
            txtIdMove.Enabled = id;
        }
        private void BtnSalir_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void FrmMovimiento_Load(object sender, EventArgs e)
        {
            Unable(true, true, false, false);
            _move.llenarComboP(cmbPartida, "Select idpartida from Partida", "Partida");
            cmbPartida.Text = "";
            Actualizar();
        }
        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            Actualizar();
        }
        private void DtgMovimientos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            fila = e.RowIndex;
            txtIdMove.Text = DtgMovimientos.Rows[fila].Cells[1].Value.ToString();
            cmbPartida.Text = DtgMovimientos.Rows[fila].Cells[0].Value.ToString();
            txtMovimiento.Text = DtgMovimientos.Rows[fila].Cells[2].Value.ToString();
            txtComentario.Text = DtgMovimientos.Rows[fila].Cells[3].Value.ToString();
            Unable(false, false, true, true);
        }
        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                _move.Insertar(new Movimientos(int.Parse(cmbPartida.Text), int.Parse(txtIdMove.Text), txtMovimiento.Text, txtComentario.Text));
                Actualizar();
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Caracteres invalidos" + ex);
            }
        }
        private void BtnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                _move.Actualizar(new Movimientos(int.Parse(cmbPartida.Text), int.Parse(txtIdMove.Text), txtMovimiento.Text, txtComentario.Text));
                Actualizar();
                Limpiar();
                Unable(true, true, false, false);
            }
            catch (Exception)
            {
                MessageBox.Show("Antes de actualizar, necesitas hacer click en el dato que deseas modificar.");
            }
        }
        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                DialogResult rs = MessageBox.Show("¿Esta seguro de eliminar al movimiento " + txtMovimiento.Text + "?",
                "Atencion!!!", MessageBoxButtons.YesNo);
                if (rs == DialogResult.Yes)
                {
                    _move.Borrar(new Movimientos(0, int.Parse(txtIdMove.Text), "", ""));
                    Limpiar();
                    Actualizar();
                    Unable(true, true, false, false);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Antes de eliminar necesitas hacer click en el dato que deseas eliminar.");
            }
        }
    }
}
