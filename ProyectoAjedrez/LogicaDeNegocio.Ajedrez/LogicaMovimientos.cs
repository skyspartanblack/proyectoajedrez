﻿using System.Data;
using AccesoDatos.Ajedrez;
using Entidades.Ajedrez;
using System.Windows.Forms;

namespace LogicaDeNegocio.Ajedrez
{
    public class LogicaMovimientos
    {
        AccesoDatosMovimientos mov = new AccesoDatosMovimientos();
        public void Insertar(Movimientos move)
        {
            mov.Insertar(move);
        }

        public void Actualizar(Movimientos move)
        {
            mov.Actualizar(move);
        }

        public void Borrar(Movimientos move)
        {
            mov.Borrar(move);
        }

        public DataSet Mostrar(string script, string tabla)
        {
            return mov.Listado(script, tabla);
        }
        public void llenarComboP(ComboBox combo, string script, string tabla)
        {
            combo.DataSource = mov.Listado(script, tabla).Tables[0];
            combo.DisplayMember = "idpartida";
        }
    }
}
