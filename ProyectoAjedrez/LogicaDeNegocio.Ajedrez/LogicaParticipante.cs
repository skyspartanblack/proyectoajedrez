﻿using System.Data;
using System.Windows.Forms;
using AccesoDatos.Ajedrez;
using Entidades.Ajedrez;

namespace LogicaDeNegocio.Ajedrez
{
    public class LogicaParticipante
    {
        AccesoDatosParticipante pa = new AccesoDatosParticipante();

        public int GetIdPais(string pais)
        {
            return pa.GetIdPais(pais);
        }

        public string GetNombrePais(int idpais)
        {
            return pa.GetNombrePais(idpais);
        }

        public void Insertar(Participante participantes)
        {
            pa.Insertar(participantes);
        }

        public void Actualizar(Participante participantes)
        {
            pa.Actualizar(participantes);
        }

        public void Borrar(Participante participantes)
        {
            pa.Borrar(participantes);
        }

        public DataSet Mostrar(string script, string tabla)
        {
            return pa.Listado(script, tabla);
        }

        public void LlenarPais(ComboBox combo, string q, string tabla)
        {
            pa.LlenarPais(combo, q, tabla);
        }
    }
}
