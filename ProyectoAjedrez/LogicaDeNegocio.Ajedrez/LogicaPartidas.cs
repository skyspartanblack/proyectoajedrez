﻿using System.Data;
using AccesoDatos.Ajedrez;
using Entidades.Ajedrez;
using System.Windows.Forms;

namespace LogicaDeNegocio.Ajedrez
{
    public class LogicaPartidas
    {
        AccesoDatosPartidas _PartidasAccesoDatos = new AccesoDatosPartidas();
        public void Guardar(Partidas partida)
        {
            _PartidasAccesoDatos.GuardarPartida(partida);
        }
        public void Actualizar(Partidas partida)
        {
            _PartidasAccesoDatos.ActualizarPartida(partida);
        }
        public void Eliminar(Partidas partida)
        {
            _PartidasAccesoDatos.EliminarPartida(partida);
        }
        public DataSet Obtener(string consulta, string tabla)
        {
            return _PartidasAccesoDatos.ObtenerPartidas(consulta, tabla);
        }
        public void LlenarSala(ComboBox combo, string consulta, string tabla)
        {
            _PartidasAccesoDatos.LLenarSala(combo, consulta, tabla);
        }
        public void LlenarArbitro(ComboBox combo, string consulta, string tabla)
        {
            _PartidasAccesoDatos.LlenarArbitro(combo, consulta, tabla);
        }
        public void LlenarHotel(ComboBox combo, string consulta, string tabla)
        {
            _PartidasAccesoDatos.LlenarHotel(combo, consulta, tabla);
        }
    }
}
