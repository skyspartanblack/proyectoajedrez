﻿using Entidades.Ajedrez;
using AccesoDatos.Ajedrez;
using System.Data;
using System.Windows.Forms;

namespace LogicaDeNegocio.Ajedrez
{
    public class LogicaCampeonato
    {
        AccesoDatosCampeonato camp = new AccesoDatosCampeonato();

        public void Insertar(Campeonato campp)
        {
            camp.Insertar(campp);
        }

        public void Actualizar(Campeonato campp)
        {
            camp.Actualizar(campp);
        }

        public void Borrar(Campeonato campp)
        {
            camp.Borrar(campp);
        }

        public DataSet Mostrar(string script, string tabla)
        {
            return camp.Listado(script, tabla);
        }
        public void llenarComboP(ComboBox combo, string script, string tabla)
        {
            combo.DataSource = camp.Listado(script, tabla).Tables[0];
            combo.DisplayMember = "nombre";
        }
        public string ObtenerParticipante(int partida)
        {
            return camp.TraerParticipante(partida);
        }
        public int ObtenerId(string par)
        {
            return camp.TraerIdParticipante(par);
        }
    }
}
