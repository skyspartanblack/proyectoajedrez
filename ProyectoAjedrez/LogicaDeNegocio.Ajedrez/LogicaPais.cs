﻿using AccesoDatos.Ajedrez;
using Entidades.Ajedrez;
using System.Data;
using System.Windows.Forms;

namespace LogicaDeNegocio.Ajedrez
{
    public class LogicaPais
    {
        AccesoDatosPaises _PaisesAccesoDatos = new AccesoDatosPaises();
        public void Guardar(Paises pais)
        {
            _PaisesAccesoDatos.GuardarPais(pais);
        }
        public void Actualizar(Paises pais)
        {
            _PaisesAccesoDatos.ActualizarPais(pais);
        }
        public void Eliminar(Paises pais)
        {
            _PaisesAccesoDatos.EliminarPais(pais);
        }

        public DataSet Obtener(string consulta, string tabla)
        {
            return _PaisesAccesoDatos.ObtenerPaises(consulta, tabla);
        }
        public int ObtenerIDPais(string nombre)
        {
            return _PaisesAccesoDatos.GetIdPais(nombre);
        }
        public string ObtenerNombrePais(int idpais)
        {
            return _PaisesAccesoDatos.GetPais(idpais);
        }
        public void Llenarcombo(ComboBox combo, string consulta, string tabla)
        {
            _PaisesAccesoDatos.LlenarPaises(combo, consulta, tabla);
        }
    }
}
