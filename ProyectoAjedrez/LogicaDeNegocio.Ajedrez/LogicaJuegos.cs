﻿using AccesoDatos.Ajedrez;
using Entidades.Ajedrez;
using System.Windows.Forms;
using System.Data;

namespace LogicaDeNegocio.Ajedrez
{
    public class LogicaJuegos
    {
        AccesoDatosJuegos _JuegosAccesoDatos = new AccesoDatosJuegos();
        public void Guardar(Juegos juego)
        {
            _JuegosAccesoDatos.GuardarJuego(juego);
        }
        public void Actualizar(Juegos juego)
        {
            _JuegosAccesoDatos.ActualizarJuego(juego);
        }
        public void Eliminar(Juegos juego)
        {
            _JuegosAccesoDatos.EliminarJuego(juego);
        }
        public DataSet Obtener(string consulta, string tabla)
        {
            return _JuegosAccesoDatos.ObtenerJuegos(consulta, tabla);
        }
        public void LlenarPartida(ComboBox combo, string consulta, string tabla)
        {
            _JuegosAccesoDatos.LLenarPartida(combo, consulta, tabla);
        }
        public void LlenarJugador(ComboBox combo, string consulta, string tabla)
        {
            _JuegosAccesoDatos.LlenarJugador(combo, consulta, tabla);
        }
    }
}
